import unittest
import mock
import wordcount

class TestWordCount(unittest.TestCase):
    @mock.patch("sys.stdin.readlines", create=True, new=mock.MagicMock(return_value=["a b c"]))
    def test_one_line(self):
        with mock.patch("sys.stdout") as stdout_mock:
            wordcount.main()
            stdout_mock.write.assert_any_call("3")

if __name__ == "__main__":
     unittest.main()
