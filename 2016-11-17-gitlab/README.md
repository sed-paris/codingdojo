# Coding-dojo @ Inria de Paris 2016-11-17: gitlab.inria.fr

![Gitlab Home](gitlab-home.png)
---
## Read the doc!

- Local doc:
https://gitlab.inria.fr/siteadmin/doc/wikis/home

- Gitlab doc:
https://gitlab.inria.fr/help

---
## Support

- https://helpdesk.inria.fr (with an Inria account)

- sed-forge.helpdesk-prc@inria.fr
---
## Account
- iLDAP: one account per LDAP login

- non-Inria users: https://gitlab-account.inria.fr
one account per e-mail address

---
## Limits: 50 projects/user, 2Go data/project
![Gitlab Limits](limits.png)
---
## Security
### SSH key

### Two-factor authentication

Google Play: Google Authenticator
Recovery tokens

### Tokens

Access to the API
---
## Command line instructions

Profile settings->Access Tokens


Your New Personal Access Token
Copy to Clipboard button on the right

```
gem install gitlab
export GITLAB_API_ENDPOINT='https://gitlab.inria.fr/api/v3'
export GITLAB_API_PRIVATE_TOKEN='<token>'
```
---
## Gitlab API

http://www.rubydoc.info/gems/gitlab

python-gitlab

https://gitlab.inria.fr/siteadmin/gitlab_import_ildap_group_members

```
gitlab create_ssh_key "My key" "`cat ~/.ssh/id_rsa.pub`"
```
---
## New project
Dashboard Projects New Project
Project path: fixed part, user or group,

Import project from
- GitHub
- Git Repo by URL
- GitLab export

Project description
appears as sub head line on the project page and on the list of projects

Visibility Level
- Private (0)
Project access must be granted explicitly to each user.
- Internal (10)
The project can be listed and cloned by any logged in user.
- Public (20)
The project can be listed and cloned without any authentication.

```
gitlab create_project <project name> \
  "{ description: <description>, visibility_level: <level> }"
```

---
## Import from GitHub

Settings->Personnal access tokens->
Copy Token button

Repository, Issues...

Issues attributed to the importer when the user is unknown
---
## Set more than one email address

Profile settings->Emails

```
gitlab add_email <email>
```
---
## Emails on push

Services->Emails on push

https://github.com/gitlabhq/gitlabhq/blob/master/doc/api/services.md

```
gitlab project_search <project name>
gitlab change_service project-id emails-on-push \
  "{ recipients: 'email' }"
```

---
## Issue tracker

Issues->New Issue

```
gitlab create_issue <project-id> "Issue title" \
  "{ description: '...', assignee_id: '...' }"

gitlab issues <project-id>

gitlab edit_issue <project-id> <issue-id> \
  "{ title: '...', assignee_id: '...' }"

gitlab close_issue <project-id> <issue-id>
```

https://gitlab.inria.fr/help/user/project/issues/automatic_issue_closing.md

clos{e,es,ed,ing}, fix{e,es,ed,ing}, resolv{e,es,ed,ing} #n

Reopen issue (`gitlab reopen_issue <project-id> <issue-id>`)
---
## Wiki

Each page can be in Markdown, RDoc, AsciiDoc.

The starting page has the page slug "home".

[Link Title](page-slug)

Git repository: <Project URL>.wiki.git
---
## Roles and permissions
https://gitlab.inria.fr/help/user/permissions

- Guest
  - private project: issue, comments, wiki, download builds
  - internal/public project: read access to the code
- Reporter: manage issue tracker, see merge requests
- Developer: write access to the code, create new branches, create new merge requests
- Master: add team members, manage runners, push to protected branches, tags
- Owner: move/remove the project
---
## Groups
Transfer project Namespace

Permissions for group members.

If a user is both in a group's project and the project itself, the highest permission level is used.

---
## Fork

Only to another namespace (or rename and transfer)

Remove fork relationship
```
gitlab create_fork <project id>
```
---
## Merge Request

Merge Requests->New Merge Request

Target project: current project or a fork ancestor
Branch
---
## Keyboard shortcut

Key `?` for help

- `g p` project home page
- `g i` issues
- `g m` merge requests
- `i` new issue
---
## Protected branches

master is protected by default:
Masters are the only one allowed to merge and to push.

support for wildcards

- Allowed to merge: Masters or Developers+Masters
- Allowed to push: Masters or Developers+Masters or No one

---
## Continuous integration with Jenkins
Check Poll SCM and put in Schedule
# Leave empty. We don't poll periodically, but need
# polling enabled to let HTTP trigger work
In Gitlab project settings, Webhooks, put
http://ci.inria.fr/<name of the project>/git/notifyCommit?url=<git repo URL>
---
## Continuous integration with Runners

Install GitLab Runner
https://docs.gitlab.com/runner/install/

Community slave images on ci.inria.fr:
- sedparis-ubuntu-16.04-64-runner
- sedparis-osx-10.9-runner
- sedparis-windows7-64-sshd-runner

```
sudo gitlab-ci-multi-runner register
c:\Multi-Runner\gitlab-ci-multi-runner register
```

Coordinator URL: https://gitlab.inria.fr/ci
Registration token: Runners
---
## .gitlab-ci.yml

File `.gitlab-ci.yml` in the root of the project

```
before_script:
- command

task1:
  script:
    - command
```

---
## Only allow succesful merge

Edit project

Only allow merge requests to be merged if the build succeeds
